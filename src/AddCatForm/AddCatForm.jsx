const AddCatForm = () => {
    return (
        <div>
            <h1>This is the add cat form</h1>
            <form>
                <div>
                <label>
                    Name:
                    <input
                    type="text"
                    name="name"
                    />
                </label>
                </div>
                <div>
                <label>
                    Photo:
                    <input
                    type="text"
                    name="image"
                    />
                </label>
                </div>
                <input type="submit" value="Submit" />
            </form>
        </div>
    )
}

export default AddCatForm;