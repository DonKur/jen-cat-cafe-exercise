import "./App.css";
import { Route } from "react-router-dom";
import CatProfile from "./CatProfile/CatProfile";
import AddCatForm from './AddCatForm/AddCatForm'


function App() {
  return (
    <div className="App">
      <Route exact path="/cats" component={CatProfile} />
      <Route exact path="/add-cat" component={AddCatForm} />
    </div>
  );
}

export default App;
